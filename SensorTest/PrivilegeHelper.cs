﻿using System;
using Tizen;
using Tizen.Security;

namespace SensorTest
{
    public class PrivilegeHelper
    {
        /// <summary>
        /// Checks a specified privileged.
        /// </summary>
        /// <param name="privilege">The privilege to check.</param>
        /// <param name="granted">Privilege granted callback.</param>
        /// <param name="denied">Privilege denied callback.</param>
        public static void CheckPrivilege(string privilege, Action granted, Action denied = null)
        {
            // Check if the user has permission for the privilege
            var result = PrivacyPrivilegeManager.CheckPermission(privilege);

            switch (result)
            {
                case CheckResult.Allow:
                    // Invoke granted callback
                    granted?.Invoke();
                    break;

                case CheckResult.Ask:
                    // Ask user for permission
                    AskPermission(privilege, granted, denied);
                    break;

                default:
                    // Invoke denied callback
                    denied?.Invoke();
                    break;
            }
        }

        /// <summary>
        /// Asks the user for permission to a specified privilege.
        /// </summary>
        /// <param name="privilege">The privilege to ask the user's permission for.</param>
        /// <param name="granted">The privilege granted callback.</param>
        /// <param name="denied">The privilege denied callback.</param>
        private static void AskPermission(string privilege, Action granted, Action denied)
        {
            // Check if getting the response context of the privilege manager succeeded
            if (!PrivacyPrivilegeManager.GetResponseContext(privilege).TryGetTarget(out var context))
                // Could not get response context, cant ask for permission
                return;

            // Hook callback
            context.ResponseFetched += (sender, e) =>
            {
                // Calculate the result
                var result = e.cause == CallCause.Answer && e.result == RequestResult.AllowForever;
                // Log some debug info
                Log.Debug(nameof(SensorManager), $"Received permission response. privilege={privilege}, result={result}");

                // Check if the user has permission
                if (result)
                    // Invoke the granted callback
                    granted?.Invoke();
                else
                    // Invoke the denied callback
                    denied?.Invoke();
            };

            // Log some debug info
            Log.Debug(nameof(SensorManager), $"Asking user for permission. privilege={privilege}");
            // Request the user for permission
            PrivacyPrivilegeManager.RequestPermission(privilege);
        }
    }
}
