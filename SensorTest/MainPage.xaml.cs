﻿using Xamarin.Forms.Xaml;
using Tizen.Wearable.CircularUI.Forms;

namespace SensorTest
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : CirclePage
    {
        public MainPage()
        {
            InitializeComponent();

            var h = new SensorManager();

            // TODO: do something with the sensor manager
        }
    }
}