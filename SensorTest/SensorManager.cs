﻿using Tizen.Sensor;

namespace SensorTest
{
    class SensorManager
    {
        private const string healthInfoPrivilege = "http://tizen.org/privilege/healthinfo";

        private readonly Accelerometer accelerometer;
        private readonly Gyroscope gyroscope;

        private HeartRateMonitor heartRateMonitor;

        /// <summary>
        /// Constructs an instance of the <see cref="SensorManager"/> class.
        /// </summary>
        public SensorManager()
        {
            // Check the health info privilege (omit the last parameter to keep the application alive and simply not use the heart rate monitor)
            PrivilegeHelper.CheckPrivilege(healthInfoPrivilege, OnHealthInfoPrivilegeGranted, OnPrivilegeDenied);

            // Instantiate the accelerometer
            accelerometer = new Accelerometer();
            // Instantiate the gyroscope
            gyroscope = new Gyroscope();

            // Hook update event
            accelerometer.DataUpdated += OnAccelerometerUpdated;
            // Hook update event
            gyroscope.DataUpdated += OnGyroscopeUpdated;

            // TODO: configure the accelerometer
            // TODO: configure the gyroscope
        }


        /// <summary>
        /// Privilege denied callback. Closes the entire application.
        /// </summary>
        private void OnPrivilegeDenied() => Tizen.Applications.Application.Current.Exit();

        /// <summary>
        /// HearthRateMonitor privilege granted. Instantiates the heart rate monitor.
        /// </summary>
        private void OnHealthInfoPrivilegeGranted()
        {
            heartRateMonitor = new HeartRateMonitor();

            // Hook update event
            heartRateMonitor.DataUpdated += OnHearthRateMonitorUpdated;

            // TODO: configure the heart rate monitor
        }


        /// <summary>
        /// Gyroscope updated event handler.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void OnGyroscopeUpdated(object sender, GyroscopeDataUpdatedEventArgs e)
        {
            // TODO: handle data
        }

        /// <summary>
        /// Accelerometer updated event handler.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void OnAccelerometerUpdated(object sender, AccelerometerDataUpdatedEventArgs e)
        {
            // TODO: handle data
        }

        /// <summary>
        /// Heart rate monitor updated event handler.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        private void OnHearthRateMonitorUpdated(object sender, HeartRateMonitorDataUpdatedEventArgs e)
        {
            // TODO: handle data
        }
    }
}
